# Use a Rust base image
FROM rust:latest as build

# Set the working directory inside the container
WORKDIR /app

# Copy the Cargo.toml and Cargo.lock files to leverage Docker's layer caching
COPY Cargo.toml Cargo.lock ./

COPY static/ /app/static/

# Copy the source code of your application
COPY src/ ./src/

# Build your application
RUN cargo build --release

# Use a newer Debian slim image for the final container to match glibc version
FROM debian:bookworm-slim

# Set the working directory inside the container
WORKDIR /app

# Copy the built executable from the build stage
COPY --from=build /app/target/release/hello-world .

# Expose the port your application runs on
EXPOSE 8080

# Command to run your application
CMD ["./hello-world"]